'use strict';
{
  let serverURL;
  if (location.hostname === 'localhost') {
    serverURL = 'http://localhost:3000';
  } else {
    serverURL = 'https://hextech-champions.herokuapp.com';
  }
  let loading;
  let data;

  let form;

  let summonerNameInput;
  let platformSelect;
  let searchButton;
  let filterOutput;
  let championsOutput;

  let progress;
  let championTemplate;

  function XHRPromise(url, response_handler) {
    return new Promise((resolve, reject) => {
      const request = new XMLHttpRequest();
      request.open('GET', url);
      request.responseType = 'json';
      request.onload = () => {
        if (request.status === 200) {
          resolve(response_handler(request.response));
        } else {
          reject(new Error('Response status:' + request.statusText));
        }
      };
      request.onerror = event => {
        reject(event);
      };
      request.send();
    });
  }

  const championsPromise = XHRPromise(
    'https://ddragon.leagueoflegends.com/api/versions.json',
    versions => versions[0]
  )
  .then(version => XHRPromise(
    `https://ddragon.leagueoflegends.com/cdn/${version}/data/en_US/champion.json`,
    response => new Map(
      Object
        .values(response.data)
        .reduce((pairs, champion) => [...pairs, [parseInt(champion.key, 10), champion]], [])
    )
  ))
  .catch(error => {
    setError('There was a network error.');
  });

  function waitForDOM() {
    return new Promise(resolve => {
      if (document.readyState !== 'loading') {
        resolve();
      } else {
        document.addEventListener('DOMContentLoaded', resolve);
      }
    })
  }

  function getFromDOM() {
    form = document.querySelector('form');
    summonerNameInput = form.elements.summonerName;
    platformSelect = form.elements.platform;
    searchButton = form.elements.search;
    filterOutput = form.elements.filter;
    championsOutput = document.getElementById('champions');

    progress = document.querySelector('progress');
    championTemplate = document.getElementById('champion');
  }

  function observeDOM() {
    // TODO: can this fire before waitForDOM resolves?
    form.addEventListener('submit', submit);
  }

  function setHidden(element, value) {
    element.classList.toggle('hidden', value);
  }

  function setSummonerName(value) {
    summonerNameInput.value = value;
  }

  function setPlatform(value) {
    platformSelect.value = value;
  }

  function setLoading(value) {
    loading = value;
    searchButton.disabled = loading;
    setHidden(progress, !loading);
    setHidden(filterOutput, !data || loading);
    setHidden(championsOutput, !data || loading);
  }

  function setError(value) {
    if (!value) {
      return;
    }
    alert(value);
  }

  function setData(masteries, champions) {
    data = masteries;
    if (!data) {
      return;
    }
    filterOutput.value = masteries.map(champion => champions.get(champion.championId).name).join('|');
    // let not const because firefox bug https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of#Browser_compatibility
    for (let mastery of masteries) {
      const element = document.importNode(championTemplate.content, true);
      const champion = champions.get(mastery.championId);
      element.querySelector('figcaption').textContent = champion.name;
      const imgElement = element.querySelector('img');
      imgElement.src = `https://ddragon.leagueoflegends.com/cdn/img/champion/loading/${champion.id}_0.jpg`;
      imgElement.alt = champion.name;
      championsOutput.appendChild(element);
    }
  }

  function getFromStorage() {
    const storedSummonerName = localStorage.getItem('summonerName');
    if (storedSummonerName) {
      setSummonerName(storedSummonerName);
    }
    const storedPlatform = localStorage.getItem('platform');
    if (storedPlatform) {
      setPlatform(storedPlatform);
    }
  }

  function submit(event) {
    if (event) {
      event.preventDefault();
    }
    const summonerName = summonerNameInput.value;
    const platform = platformSelect.value;
    if (!platform || !summonerName) {
      return;
    }
    setLoading(true);
    function stopLoading() {
      setLoading(false);
    }

    Promise.all([
      XHRPromise(`${serverURL}/api/${platform}/${summonerName}`, x => x),
      championsPromise,
    ])
      .then(([masteries, champions]) => {
        const { error } = masteries;
        if (error) {
          stopLoading();
          setError(error);
          setData(null);
          return;
        }

        if (masteries.length === 0) {
          stopLoading();
          setError('No eligible Champion found.');
          setData(null);
          return;
        }

        setData(masteries, champions);
        setError(null);
        stopLoading();
      }, error => {
        stopLoading();
        setError('There was a network error.');
        setData(null);
      });

    try {
      localStorage.setItem('platform', platform);
      localStorage.setItem('summonerName', summonerName);
    } catch (_) { }
  }

  waitForDOM()
    .then(getFromDOM)
    .then(observeDOM)
    .then(getFromStorage)
    .then(submit);
}
