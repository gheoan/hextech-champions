# Setup

* Install [`Node.js`](https://nodejs.org/en/download/current/) (atleast version `8.2.0`) along with
`npm`.
* Upgrade `npm` to atleast version `5.4.1`.
* Run `npm install` to install dependencies.
* Edit `./src/index.js` so that the `serverURL` variable is the address of your server.

# Building

* Run `npm run build` to process (minify, add revision numbers) the source files from the `src`
directory and output them to the `dist` directory.

# Development

* Run `npm run start-dev` to start a server using the raw source files (the `src` directory).
* Run `npm run start-prod` to start a server using the processed source files (the `dist` directory)
. Make sure to build first.

# Deployment

This project is using Firebase Hosting.
* Run `npx firebase login`.
* Run `npx firebase use --add`.
* Run `npm run deploy` to build then deploy to Firebase Hosting.
