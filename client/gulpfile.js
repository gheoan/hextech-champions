const { src, dest: dist, task, series } = require('gulp');
const { join: joinPaths } = require('path');
const filter = require('gulp-if');
let minifyCSS = require('gulp-cssnano');
minifyCSS = minifyCSS.bind(minifyCSS, { safe: true });
let minifyJS = require('gulp-babel');
minifyJS = minifyJS.bind(minifyJS, { presets: ['minify'] });
let minifyHTML = require('gulp-htmlmin');
minifyHTML = minifyHTML.bind(minifyHTML, {
  removeComments: true,
  collapseWhitespace: true,
  collapseBooleanAttributes: true,
  removeAttributeQuotes: true,
  removeRedundantAttributes: false,
  removeEmptyAttributes: true,
  removeScriptTypeAttributes: true,
  removeStyleLinkTypeAttributes: true,
  caseSensitive: true,
  conservativeCollapse: true,
  removeCDATASectionsFromCDATA: true,
  removeCommentsFromCDATA: true,
  useShortDoctype: true,
  minifyCSS: true,
  minifyJS: true
});
const rev = require('gulp-rev');
const revReplace = require('gulp-rev-replace');
const del = require('del');

const paths = {
  root: '.',
  src: 'src',
  hostingConfig: 'firebase.json',
  revManifest: 'dist/rev-manifest.json',
  dist: 'dist'
}

function clean() {
  return del(joinPaths(paths.dist, '*'));
}

function revision() {
  return src(joinPaths(paths.src, '*.{css,js,png}'))
    .pipe(filter('*.css', minifyCSS()))
    .pipe(filter('*.js', minifyJS()))
    .pipe(rev())
    .pipe(dist(paths.dist))
    .pipe(rev.manifest())
    .pipe(dist(paths.dist));
}

function html() {
  return src(joinPaths(paths.src, 'index.html'))
    .pipe(revReplace({ manifest: src(paths.revManifest), replaceInExtensions: ['.html'] }))
    .pipe(minifyHTML())
    .pipe(rev())
    .pipe(dist(paths.dist))
    .pipe(rev.manifest({ merge: true }))
    .pipe(dist(paths.dist));
}

function copy() {
  return src(joinPaths(paths.src, 'riot.txt'))
    .pipe(dist(paths.dist));
}

function hostingConfig() {
  return new Promise((resolve, reject) => {
    const hostingConfig = require('./' + paths.hostingConfig);
    hostingConfig.hosting.rewrites[0].destination = '/' + require('./' + paths.revManifest)["index.html"];
    try {
      require('fs').writeFileSync(paths.hostingConfig, JSON.stringify(hostingConfig, null, '  ') + '\n');
      resolve();
    } catch (error) {
      reject(error);
    }
  }).then(del(paths.revManifest));
}

task('build', series(clean, revision, html, copy, hostingConfig));
