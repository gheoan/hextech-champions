# Setup

* Install [`Node.js`](https://nodejs.org/en/download/current/) (atleast version `7.6.0`) along with
`npm`.
* Run `npm install` to install dependencies.

# Environment variables

The server requires the `RIOTGAMES_API_KEY` environment variables to be set to Riot Games API Key
and `CLIENT_URL` to the address of the client.

# Development

* Run `npm run start` to start the server.

# Deployment

The server is using Heroku.

* Install [`Heroku CLI`](https://devcenter.heroku.com/articles/heroku-cli) and
[`git`](https://git-scm.com/downloads).
* Run `heroku login`.
* Run `heroku create` to create a new Heroku app or `heroku git:remote -a app-name` (replace
`app-name`) if you already have an app.
* Run `heroku config:set RIOTGAMES_API_KEY=your_api_key CLIENT_URL=your_client_url` to set the
environment variables
* Run `npm run deploy` to deploy to Heroku.
