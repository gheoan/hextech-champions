const Koa = require('koa');
const app = new Koa();
const compress = require('koa-compress');
const cors = require('kcors');
app.use(compress());
app.use(cors());
const port = process.env.PORT || 3000;
app.listen(port);
console.log(`Server started on http://localhost:${port}`);

const apiKey = process.env.RIOTGAMES_API_KEY;
if (!apiKey) {
  console.error('RIOTGAMES_API_KEY env variable not found.');
}
const { URL } = require('url');
function getURL(platform, path) {
  const url = new URL(path, `https://${platform}.api.riotgames.com/lol`);
  url.searchParams.set('api_key', apiKey);
  return url.toString();
}

// Routes
const router = require('koa-router')();
const fetch = require('node-fetch');

function checkStatus(obj) {
  const { status } = obj;
  if (status) {
    throw status.status_code;
  } else {
    return obj;
  }
}

router.get('/api/:platform/:summonerName', async function (context) {
  const { platform, summonerName } = context.params;

  const summonerURL = getURL(platform, `/lol/summoner/v4/summoners/by-name/${summonerName}`);
  let summonerID;
  try {
    const response = await fetch(summonerURL);
    const json = await response.json();
    // TODO: is this needed?
    checkStatus(json);
    summonerID = json.id;
  } catch (statusCode) {
    let error;
    if (statusCode === 404) {
      error = 'Summoner Name not found.';
    } else {
      error = 'Unknown error.';
    }
    context.body = { error };
    return;
  }

  const masteryURL = getURL(platform, `/lol/champion-mastery/v4/champion-masteries/by-summoner/${summonerID}`);
  try {
    const response = await fetch(masteryURL);
    const json = await response.json();
    checkStatus(json);
    context.body = json.reduce((champions, champion) => {
      if (!champion.chestGranted) {
        const {
          championId,
          championLevel,
        } = champion;

        champions.push({
          championId,
          championLevel,
        });
      }

      return champions;
    }, []).slice(0, 12);
  } catch (_) {
    context.body = { error: 'Unknown error.' };
    return;
  }
});
app.use(router.routes());

app.use(async function ({response}) {
  response.status = 301;
  response.redirect(process.env.CLIENT_URL);
});
