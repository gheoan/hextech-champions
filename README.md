# Hextech Champions

Hextech Champions helps League of Legends players earn Hextech Chests by recommending them champions
they are good at but have not obtained a chest for yet.

[**Demo**](https://hextech-champions.web.app/)

## Building

See `client/README` and `server/README` for instructions on how to build.

## About

### Why

As a free-to-play League of Legends player I am constantly looking to earn Hextech Crafting rewards.
That means I am always looking at the champions tab in my profile to see what champions are eligible
to earn Hextech Chests. I also wanted to see champion eligibility in champion select or when I am in
the game.

### How

Hextech Crafting Assistant uses the Champion Mastery API to get data about a Summoner's champions.
It then filters out champions that already got a Hextech Chest and sorts them based on mastery level
.
